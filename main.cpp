﻿#include <iostream>
#include <string>

using namespace std;

//As C porgrams are mostly using ASCII charachter set following replacements are used
//∨ is replaced by |
//∧ is is replaced by &
//¬ is replaced by ~
//-> is replaced by >
// ↔ is replaced by =
//α  is replaced by a
//β  is replaced by b
//γ  is replaced by c
//δ  is replaced by d

//Evaluates true or false of an assignment
bool eval(char *cstr, int start, int end){
   if (start > end){ // Null string
      //cout << start <<" "<<end << " "<< "false start > end" << endl;
      return false;
   }
   if (start == end){
      if ((*(cstr + start) >= 48 && *(cstr + start) <= 57)){
         //cout << "decoding p" << *(cstr + end)<<" "<< start << " " << end << " " << endl;
         return (*(cstr + end)) - 48;
      }
   }
   if (*(cstr + start) == '(' && *(cstr + end) == ')'){ //All forms not prepostions must start this way
      if (*(cstr + start + 1) == '~'){
         //cout << "negation at " << (start + 1) << endl;
         return (!eval(cstr, start + 2, end - 1)); //Unary connection recursive call
      }
      else{
         int excess_left_bracket_count = 1; //For the already detected left bracket
         int i = start + 1;
         for (i = start + 1; i <= end; i++){
            if (*(cstr + i) == '('){
               excess_left_bracket_count++;
            }
            else if (*(cstr + i) == ')'){
               excess_left_bracket_count--;
            }
            else if (excess_left_bracket_count == 1){
               if (*(cstr + i) == '|'){
                  //cout << "PC | at " << i << endl;
                  bool a1 = eval(cstr, start + 1, i - 1);
                  //cout << "a1| is " << a1 << endl;
                  bool a2 = eval(cstr, i + 1, end - 1);
                  //cout << "a2 | is " << a2 << endl;
                  return a1 || a2;
               }
               if (*(cstr + i) == '&'){
                  //cout << "PC & at "<< i << endl;
                  bool a1 = eval(cstr, start + 1, i - 1);
                  //cout << "a1 & is " << i << " " << a1 << endl;
                  bool a2 = eval(cstr, i + 1, end - 1);
                  //cout << "a2 & is " << i << " " << a2 << endl;
                  return a1 && a2;
               }
               if (*(cstr + i) == '>'){
                  //cout << "PC > at " << i << endl;
                  bool a1 = eval(cstr, start + 1, i - 1);
                  //cout << "a1 > is " << i << " " << a1 << endl;
                  //cout << "~a1 > is " << i << " " << (!a1) << endl;
                  bool a2 = eval(cstr, i + 1, end - 1);
                  //cout << "a2 > is " <<i<<" "<< a2 << endl;
                  return ((!a1) || a2);
               }
               if (*(cstr + i) == '='){
                  //cout << "PC = at " << i << endl;
                  bool a1 = eval(cstr, start + 1, i - 1);
                  //cout << "a1 = is " << a1 << endl;
                  bool a2 = eval(cstr, i + 1, end - 1);
                  //cout << "a2 = is " << a2 << endl;
                  return ((a1 && a2) || ((!a1) && (!a2)));
               }
            }
         }
         if (i > end){
            //cout << start << " " << end << " " << "false No conn" << endl;
            return false; //No binary connection detected
         }
      }
   }
   else {
      //cout << "Paranthesis fault at " << start << " " << end << endl;
      return false;
   }
}

//Substities a proposition in a formula
void substitution(char *cstr, char *cstr_1 , char a, char b, int end){
   for (int i = 0; i < end; i++){
      if (*(cstr + i) == a){
         *(cstr_1 + i) = b;
      }
      else {
         *(cstr_1 + i) = *(cstr + i);
      }
   }
}

//Splitting method modified
bool split(char *cstr, int start, int end, char prop[], bool prop_assign[], int prop_len, int prop_assigned){

   char cstr_1[200];
   if (prop_assigned == prop_len){
      bool state = eval(cstr, start, end - 1);
      if (state){
         cout << "Satisfying assignment :";
         for (int i = 0; i < prop_len; i++){
            cout << prop[i] <<" = "<<prop_assign[i] << " ";
         }
         cout << endl;
      }
      return state;
   }
   if (prop_assigned < prop_len){
      substitution(cstr, cstr_1, prop[prop_assigned], '1', end);
      prop_assign[prop_assigned] = 1;
      bool split1 = split(cstr_1, start, end, prop, prop_assign, prop_len, prop_assigned + 1);
      substitution(cstr, cstr_1, prop[prop_assigned], '0', end);
      prop_assign[prop_assigned] = 0;
      bool split2 = split(cstr_1, start, end, prop, prop_assign, prop_len, prop_assigned + 1);
      return split1 | split2;
   }
}

int main(){
   //char *input = "(((~a)&(~c))>(e&((b&(~d))|((~b)&d))))";
   //char *input = "((a&(d&(~e)))>((b&c)|((~b)&(~c))))";
   //char *input = "((a&(b|e))=((c&(~d))|((~c)&d)))";
   char input[109] = "(((((~a)&(~c))>(e&((b&(~d))|((~b)&d))))&((a&(d&(~e)))>((b&c)|((~b)&(~c)))))&((a&(b|e))=((c&(~d))|((~c)&d))))";
   int end = 37 + 34 + 31 + 6;
   bool prop_assign[5];
   int prop_assigned = 0;
   int prop_len = 4;
   //For part 1
   cout << "PART 1" << endl;
   char input_1[109];
   substitution(input, input_1, 'a', '1', end);
   char prop_1[] = { 'b', 'c', 'd', 'e' };
   split(input_1, 0, end, prop_1, prop_assign, prop_len, prop_assigned);
   //For part 2
   cout << "PART 2" << endl;
   char input_2[109];
   substitution(input, input_2, 'a', '0', end);
   char prop_2[] = { 'b', 'c', 'd', 'e' };
   split(input_2, 0, end, prop_2, prop_assign, prop_len, prop_assigned);
   //For part 3
   cout << "PART 3" << endl;
   char input_3[109];
   substitution(input, input_3, 'b', '1', end);
   char prop_3[] = { 'a', 'c', 'd', 'e' };
   split(input_3, 0, end, prop_3, prop_assign, prop_len, prop_assigned);
   //For part 4
   cout << "PART 4" << endl;
   char input_4[109];
   substitution(input, input_4, 'b', '0', end);
   char prop_4[] = { 'a', 'c', 'd', 'e' };
   split(input_4, 0, end, prop_4, prop_assign, prop_len, prop_assigned);
   getchar();
   return 0;
}